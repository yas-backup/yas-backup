#!/usr/bin/env python

import os
import sys
from distutils.core import setup

setup(name='YasBackup',
      version='0.1',
      description='Yet Another Simple Backup Tool',
      author='Pedro Kiefer',
      author_email='pedro@kiefer.com.br',
      url='http://www.pedro.kiefer.com.br/yasbackup',
      
      packages=['yasbackup', 'yasbackup.core', 'yasbackup.util'],
      
      data_files=[(os.path.join(sys.prefix,'bin'), ['bin/yas-backup'])]
      
      )
