'''
yas-backup-proc

Created on: Mar 24, 2011
   @author: Pedro Kiefer <pedro@kiefer.com.br>
'''

import argparse
import os
import logging

from core.ConfigManager import ConfigManager
from core.YasBackupScheduler import YasBackupScheduler
from util.Logger import Logger

class YasBackupApp():
    def __init__(self, argv):
        self._argv = argv
        self._logger = None
        self._conf = None
    def parse_cmdline(self):
        _usage = "Usage: %prog [options] (use -h or --help for help)"
        _version = "%prog "
        _prog = "yas-backup"

        parser = argparse.ArgumentParser(description='Yet Another Simple Backup Tool', argument_default=argparse.SUPPRESS, prog=_prog)

        parser.add_argument("-c", "--config-file", dest = "config",
                          metavar = "CONFIG", default = "/etc/yas-backup/config.yaml",
                          help = "set the configuration file to use")

        parser.add_argument("-F", "--full",
            action = "store_true", dest = "full_snapshot", default = False,
            help = "create full snapshot")
        
        parser.add_argument('target', nargs='*', metavar='target',
            help='Scheduled target: daily, weekly, monthly', default = 'daily')

        self._args = parser.parse_args(self._argv[1:])
        self._config_file = self._args.config
    
    def init_config(self):
        if os.path.exists(self._config_file):
            self._conf = ConfigManager()
            self._conf.load(self._config_file)
        else:
            print("Missing config file, aborting")
            exit(1)
    
    def init_logging(self):
        if self._conf.get('log') is not None:
            log = self._conf.get('log')
            Logger.init(log.get('filename', '/var/log/yas-backup.log'), log.get('level', logging.INFO) )
        else:
            Logger.init('/var/log/yas-backup.log', logging.INFO)
        self._logger = logging.getLogger(__name__)
        self._logger.info("YasBackup Initialized ...")
        self._logger.debug("Running with args: %s", self._args)
        self._conf.initLogger()
    
    def run(self):
        self.parse_cmdline()
        self.init_config()
        self.init_logging()
        
        if isinstance(self._args.target, basestring):
            self._conf.set('schedule_target', self._args.target)
        else:
            self._conf.set('schedule_target', self._args.target[0])
        
        _sched = YasBackupScheduler(self._conf.get('general'), self._conf.get('backup-tasks'))
        
        _sched.run_tasks()

def main(argv):
    app = YasBackupApp(argv)
    exitcode = app.run()
    return exitcode
