'''
util.Misc

Created on: Mar 24, 2011
   @author: Pedro Kiefer <pedro@kiefer.com.br>
'''

import re

from string import Template
from datetime import date

def pattern_match(list, regs):
    matched = []
    for pattern in regs:
        for item in list:
            if pattern.search(item):
                matched.append(item)
    return matched

def format_filename(_format, _name, _ext=None, _dateformat = '%Y%m%d'):
    if _format is None:
        return ""
    s = Template(_format)
    _date = date.today().strftime(_dateformat)
    rtn = s.safe_substitute({'name': _name, 'ext': _ext, 'date': _date })
    return rtn

def compile_regexps(patterns):
    cpats = []
    for pat in patterns:
        pat = re.compile(pat)
        cpats.append(pat)
    return cpats
