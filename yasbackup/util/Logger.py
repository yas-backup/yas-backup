'''
util.Logger

Created on: Mar 24, 2011
   @author: Pedro Kiefer <pedro@kiefer.com.br>
'''

import os
import sys
import logging
from datetime import datetime

class Logger(object):
    '''
    Logger Object
    '''

    #create formatter
    formatter = "%(asctime)s - %(levelname)s: %(message)s"
    debug_formatter = "%(asctime)s - %(levelname)s in %(module)s.%(funcName)s(%(lineno)d): %(message)s"

    def __init__(self):
        pass
    
    @staticmethod
    def init(filename=None, level=logging.INFO):
        if filename is None:
            now = datetime.now()
            ts = now.strftime('%Y-%m-%d_%H%M%S')
            filename = '%s.%s.log' % (sys.argv[0], ts)
        
        _formatter = Logger.formatter
        if level == logging.DEBUG:
            _formatter = Logger.debug_formatter
        
        logging.basicConfig(level=level,
                        format=_formatter,
                        filename=filename,
                        filemode='w')
        