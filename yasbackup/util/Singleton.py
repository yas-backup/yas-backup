'''
util.Singleton

Created on: Mar 24, 2011
   @author: Pedro Kiefer <pedro@kiefer.com.br>
'''

class Singleton(type):
    def __init__(cls, name, bases, dic): #IGNORE:C0203
        type.__init__(cls, name, bases, dic)
        cls.instance = None

    def __call__(self, *args, **kw): #IGNORE:C0203
        if self.instance is None:
            self.instance = type.__call__(self, *args, **kw)
        return self.instance
