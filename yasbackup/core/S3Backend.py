'''
core.S3Backend

Created on: May 08, 2012
   @author: Leonardo Santos <leonardopsantos@gmail.com>
'''
import os
import subprocess
import tempfile
import logging
import sys
import string
import re

from yasbackup.util.Misc import pattern_match
from yasbackup.util.Misc import format_filename
from yasbackup.core.ConfigManager import ConfigManager
from yasbackup.util.Singleton import Singleton

sys.path.append('/usr/share/s3cmd')
from S3.Exceptions import S3UploadError, ParameterError
from S3.S3 import S3
from S3.Config import Config as S3Config
from S3.S3Uri import S3Uri

logger = logging.getLogger(__name__)

def s3_path_walker(s3, path, _dir_patterns, _file_patterns):
    for root, dirs, files in os.walk(path):
        dirs[:] = filter(lambda x, tmp=pattern_match(dirs, _dir_patterns): x not in tmp, dirs)
        files[:] = filter(lambda x, tmp=pattern_match(files, _file_patterns): x not in tmp, files)
        for f in files:
            #print root+"/"+f
            s3.add_file(os.path.join(root, f))

class S3Backend(object):
    __metaclass__ = Singleton

    def __init__(self):
        self._name = None
        self._proc = None
        self._errors = None
        self._rtncode = None
        self._conf = {}
        self._config()
        self._filelist = []
        
        if logger.isEnabledFor(logging.DEBUG):
            from guppy import hpy
            self._hpy = hpy()
    
    def _config(self):
        c = ConfigManager().get('s3')
        self._conf['file-format'] = c.get('file-format') or '${name}-${date}.${ext}'

    def setName(self, name):
        self._name = name
        
    def add_file(self, file):
        self._filelist.append(file)

    def finish(self):
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(self._hpy.heap())

    def run(self, name, fileset, dir_patterns, file_patterns):

        self.setName(name)

        for f in fileset:
            if os.path.isdir(f):
                s3_path_walker(self, f, dir_patterns, file_patterns)
            elif os.path.isfile(f):
                self.add_file(f)

        if len(self._filelist) == 0:
            raise ParameterError("Nothing to upload. Expecting a local file or directory and a S3 URI destination.")

        try:
            config_file = os.path.join(os.getenv("HOME"), ".s3cfg")
            cfg = S3Config(config_file)
            s3 = S3(cfg)

            dst_bucket = format_filename(self._conf['file-format'], self._name)

            create_bucket = True
            response = s3.list_all_buckets()
            for bucket in response["list"]:
                if dst_bucket == bucket["Name"]:
                    create_bucket = False

            destination_base_uri = S3Uri("s3://" + dst_bucket)

            if create_bucket:
                response = s3.bucket_create(destination_base_uri.bucket(), cfg.bucket_location)
                logger.info("Creating a new bucket %s for %s" % (destination_base_uri, self._name))
            else:
                logger.info("Using existing bucket %s for %s" % (destination_base_uri, self._name))
        except Exception as e:
            logger.info(e)
            return

        try:
            for f in self._filelist:
                ret = subprocess.Popen(['s3cmd', '-d', 'put', f, str(destination_base_uri)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0]
                ret = ret.decode("utf-8").splitlines()
                for st in ret:
                    if string.find(st, "MD5") != -1:
                        computed_md5 = re.search("computed=([A-Za-z0-9]*)",st).group(1)
                        received_md5 = re.search("received=\"([A-Za-z0-9]*)\"",st).group(1)
                        if computed_md5 != received_md5:
                            raise S3UploadError
            logger.info("Finished s3cmd process for %s, no MD5 matching errors" % self._name)
        except Exception as e:
            logger.info(e)
            return

        self.finish()
