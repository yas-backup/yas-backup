'''
core.BackupTask

Created on: Mar 24, 2011
   @author: Pedro Kiefer <pedro@kiefer.com.br>
'''

import os
import re
import logging

from yasbackup.core.TarBackend import TarBackend
from yasbackup.core.S3Backend import S3Backend

logger = logging.getLogger(__name__)

task_TAR = 1
task_S3 = 2
task_mapping = { 'tar'  : task_TAR,
                 'TAR'  : task_TAR,
                 's3'   : task_S3, 
                 'S3'   : task_S3 }

class BackupTask(object):
    '''
    Backup Task Object
    '''
    def __init__(self):
        self._name = None
        self._fileset = []
        self._exclude = None
        self._schedule = None
        self._dir_patterns = []
        self._file_patterns = []
        self._backend = None

    def getSchedule(self):
        return self._schedule
    
    def run(self):
        if self._fileset is None or self._backend is None:
            return
        
        logger.debug("Running task %s" % self._name)
        logger.debug("Removing dirs expression %s" % self._dir_patterns)
        logger.debug("Removing files expression %s" % self._file_patterns)
        
        self._backend.run(self._name, self._fileset, self._dir_patterns, self._file_patterns)

        logger.info("Task %s is finished" % self._name)

        
class BackupTaskFactory(object):
    @staticmethod
    def init_tasks_re(_excludes):
        if _excludes is None:
            return [], []
        
        _file_patterns = []
        _dir_patterns = []
        if 'files' in _excludes:
            if isinstance(_excludes['files'], basestring):
                _file_patterns.append(re.compile(_excludes['files']))
            else:
                for f in _excludes['files']:
                    _file_patterns.append(re.compile(f))
        if 'dirs' in _excludes:
            if isinstance(_excludes['dirs'], basestring):
                _dir_patterns.append(re.compile(_excludes['dirs']))
            else:
                for d in _excludes['dirs']:
                    print d
                    _dir_patterns.append(re.compile(d))
        return _dir_patterns, _file_patterns
    
    @staticmethod
    def create(params, parent):
        if params is None:
            return None
        
        logger.info("Creating task - %s" % params.get('name'))
        b = BackupTask()
        
        b._name = params.get('name')
        fileset = params.get('fileset')
        if isinstance(fileset, basestring):
            b._fileset.append(fileset)
        else:
            b._fileset.extend(fileset)
        
        b._schedule = params.get('schedule', 'daily')
                
        b._dir_patterns, b._file_patterns = BackupTaskFactory.init_tasks_re(params.get('exclude', None))
        b._dir_patterns.extend(parent._dir_patterns)
        b._file_patterns.extend(parent._file_patterns)

        be = task_mapping[params.get('backend')] or task_TAR

        if be == task_TAR:
            b._backend = TarBackend()
        elif be == task_S3:
            b._backend = S3Backend()
        else:
            b._backend = TarBackend()

        return b
