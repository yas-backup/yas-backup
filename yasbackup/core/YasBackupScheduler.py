'''
core.YasBackupScheduler

Created on: Mar 24, 2011
   @author: Pedro Kiefer <pedro@kiefer.com.br>
'''

import re

import logging
logger = logging.getLogger(__name__)

from yasbackup.core.ConfigManager import ConfigManager
from yasbackup.core.BackupTask import BackupTask, BackupTaskFactory

class YasBackupScheduler(object):
    '''
    YasBackupScheduler
    '''
    
    def __init__(self, general, tasks):
        self._tasks = []
        self._file_patterns = []
        self._dir_patterns = []
        self._conf = ConfigManager()
        
        self.init_exclude_regs()
        
        self.create_tasks(tasks)
    
    def init_exclude_regs(self):
        c = self._conf.get('general')
        _excludes = c.get('exclude', None)
        if _excludes:
            if 'files' in _excludes:
                if isinstance(_excludes['files'], basestring):
                    self._file_patterns.append(re.compile(_excludes['files']))
                else:
                    for f in _excludes['files']:
                        self._file_patterns.append(re.compile(f))
            if 'dirs' in _excludes:
                if isinstance(_excludes['dirs'], basestring):
                    self._dir_patterns.append(re.compile(_excludes['dirs']))
                else:
                    for d in _excludes['dirs']:
                        self._dir_patterns.append(re.compile(d))
        
    def create_tasks(self, tasks):
        logger.debug("Creating tasks %s" % tasks)
        if tasks:
            for task in tasks:
                self._tasks.append(BackupTaskFactory.create(task, self))
    
    def run_tasks(self):
        tasks = []
        target = self._conf.get('schedule_target')
        for task in self._tasks:
            if task.getSchedule() == target:
                tasks.append(task)
        logger.info("Running tasks [%s] for target: %s" % (', '.join([t._name for t in tasks]), target))
        for task in tasks:
            task.run()
