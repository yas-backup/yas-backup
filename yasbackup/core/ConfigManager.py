'''
core.ConfigManager

Created on: Mar 24, 2011
   @author: Pedro Kiefer <pedro@kiefer.com.br>
'''

import yaml
import logging

class ConfigManager(object):
    '''
    Configuration Manager
    '''
    # config is a borg...
    __shared_state = {'_data' : {}, '_initialized': False, '_logger': None}
    def __init__(self):
        self.__dict__ = self.__shared_state
      
    def load(self, config):
        try:
            self._data = yaml.load(file(config, 'r'))
        except yaml.YAMLError, exc:
            print "Error in configuration file:", exc
        self._initialized = True
  
    def get(self, k):
        if not self._initialized:
            return None
        return self._data.get(k, None)

    def set(self,k,v):
        if k in self._data and self._logger:
            self._logger.info("Overwriting config %s with %s" % [k, v])
        self._data[k] = v 
    
    def initLogger(self):
        self._logger = logging.getLogger(__name__)
