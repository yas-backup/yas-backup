'''
core.TarBackend

Created on: Mar 24, 2011
   @author: Pedro Kiefer <pedro@kiefer.com.br>
'''
import os
import subprocess
import tempfile
import logging

from yasbackup.util.Misc import pattern_match
from yasbackup.util.Misc import format_filename
from yasbackup.core.ConfigManager import ConfigManager
from yasbackup.util.Singleton import Singleton

logger = logging.getLogger(__name__)


GZIP = 1
BZIP2 = 2
TAR = 3
type_mapping = { 'tar'  : TAR,
                 'gz'   : GZIP,
                 'tgz'  : GZIP, 
                 'bz2'  : BZIP2,
                 'gzip' : GZIP,
                 'bzip2': BZIP2 }

def _format_extension(type):
        if type == TAR:
            return 'tar'
        elif type == GZIP:
            return 'tar.gz'
        elif type == BZIP2:
            return 'tar.bz2'

def read_file(file):
    if os.path.exists(file):
        f = open(file, "r")
        content = f.read()
        f.close()
        return content

def tar_path_walker(tar, path, _dir_patterns, _file_patterns):
    for root, dirs, files in os.walk(path):
        dirs[:] = filter(lambda x, tmp=pattern_match(dirs, _dir_patterns): x not in tmp, dirs)
        files[:] = filter(lambda x, tmp=pattern_match(files, _file_patterns): x not in tmp, files)
        for f in files:
            #print root+"/"+f
            tar.add_file(os.path.join(root, f))

class TarBackend(object):
    __metaclass__ = Singleton

    def __init__(self):
        self._name = None
        self._proc = None
        self._errors = None
        self._rtncode = None
        self._conf = {}
        self._config()
        
        if logger.isEnabledFor(logging.DEBUG):
            from guppy import hpy
            self._hpy = hpy()
    
    def _config(self):
        c = ConfigManager().get('tar')
        self._conf['file-format'] = c.get('file-format') or '${name}-${date}.${ext}'
        self._conf['backup-dir'] = c.get('backup-dir') or '/var/backup'
        self._conf['format'] = type_mapping[c.get('format')] or GZIP
    
    def _format_arguments(self):
        options = ["--files-from", "-", "-c"]
        type = self._conf['format']
        
        if type == TAR:
            pass
        elif type == GZIP:
            options.insert(2, '--gzip')
        elif type == BZIP2:
            options.insert(2, '--bzip2')
        else:
            raise Exception("type error")
        
        if os.path.exists(self._conf.get('backup-dir')):
            options.append("--file=%s" % os.path.join(self._conf.get('backup-dir'), self._tar_file))
        else:
            options.append("--file=%s" % self._tar_file)
        
        return options
    
    def launch_sync(self, args=None):
        self._errptr, self._errfile = tempfile.mkstemp(prefix="error_")
        logger.debug("Error file %s" % self._errfile)
        
        options = self._format_arguments()
        options.insert(0, "/bin/tar")
        
        logger.debug("Launching: %s" % options)
        
        self._proc = subprocess.Popen(options, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                          stderr=self._errptr)
        self.stdout = self._proc.stdout
        self.stdin = self._proc.stdin
    
    def setName(self, name):
        self._name = name
        self._tar_file = format_filename(self._conf['file-format'], self._name, _format_extension(self._conf['format']))
        
    def add_file(self, file):
        self.stdin.write(file + "\n")
        
    def finish(self):
        self._out_errors = self._proc.communicate()
        
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(self._hpy.heap())
        
        self._rtncode = self._proc.returncode
        logger.info("Finished tar process for %s with code %d" % (self._name, self._rtncode))
        os.close(self._errptr)
        
        self._errors = read_file(self._errfile) 
        logger.debug("Errors: %s" % self._errors)
        logger.debug(self._out_errors)
        
        # Clean up
        os.unlink(self._errfile)

    def run(self, name, fileset, dir_patterns, file_patterns):
        self.setName(name)
        self.launch_sync()

        for f in fileset:
            if os.path.isdir(f):
                tar_path_walker(self, f, dir_patterns, file_patterns)
            elif os.path.isfile(f):
                self.add_file(f)

        self.finish()

